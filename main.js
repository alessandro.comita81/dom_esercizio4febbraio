console.log(document);


let articles =
[
    {'subtitle': 'sub1', 'description': 'descriz1', 'url': 'https://picsum.photos/300/300'},
    {'subtitle': 'sub2', 'description': 'descriz2', 'url': 'https://picsum.photos/301/302'},
    {'subtitle': 'sub3', 'description': 'descriz3', 'url': 'https://picsum.photos/303/303'},
    {'subtitle': 'sub4', 'description': 'descriz4', 'url': 'https://picsum.photos/304/304'},
    {'subtitle': 'sub5', 'description': 'descriz5', 'url': 'https://picsum.photos/305/305'},
    {'subtitle': 'sub6', 'description': 'descriz6', 'url': 'https://picsum.photos/306/306'},
    {'subtitle': 'sub1', 'description': 'descriz1', 'url': 'https://picsum.photos/300/307'},
    {'subtitle': 'sub2', 'description': 'descriz2', 'url': 'https://picsum.photos/301/308'},
    {'subtitle': 'sub3', 'description': 'descriz3', 'url': 'https://picsum.photos/303/309'},
    {'subtitle': 'sub4', 'description': 'descriz4', 'url': 'https://picsum.photos/304/310'},
    {'subtitle': 'sub5', 'description': 'descriz5', 'url': 'https://picsum.photos/305/311'},
    {'subtitle': 'sub6', 'description': 'descriz6', 'url': 'https://picsum.photos/306/312'},
    {'subtitle': 'sub1', 'description': 'descriz1', 'url': 'https://picsum.photos/300/313'},
    {'subtitle': 'sub2', 'description': 'descriz2', 'url': 'https://picsum.photos/301/314'},
    {'subtitle': 'sub3', 'description': 'descriz3', 'url': 'https://picsum.photos/303/315'},
    {'subtitle': 'sub4', 'description': 'descriz4', 'url': 'https://picsum.photos/304/316'},
    {'subtitle': 'sub5', 'description': 'descriz5', 'url': 'https://picsum.photos/305/317'},
    {'subtitle': 'sub6', 'description': 'descriz6', 'url': 'https://picsum.photos/306/318'},
    {'subtitle': 'sub1', 'description': 'descriz1', 'url': 'https://picsum.photos/300/319'},
    {'subtitle': 'sub2', 'description': 'descriz2', 'url': 'https://picsum.photos/301/320'},
    {'subtitle': 'sub3', 'description': 'descriz3', 'url': 'https://picsum.photos/303/321'},
    {'subtitle': 'sub4', 'description': 'descriz4', 'url': 'https://picsum.photos/304/322'},
    {'subtitle': 'sub5', 'description': 'descriz5', 'url': 'https://picsum.photos/305/323'},
    {'subtitle': 'sub6', 'description': 'descriz6', 'url': 'https://picsum.photos/306/324'}
];


let aulab = {
    'users': [
      {'name': 'Nicola', 'role': 'authenticated'},
      {'name': 'Roberto', 'role': 'guest'},
      {'name': 'Valerio', 'role': 'authenticated'},
      {'name': 'Valerio', 'role': 'guest'},
    ]
  }



  // CATTURA ELEMENTI DELLA PAGINA

  let eContainer = document.getElementById('contenitoreCard');



   

  

  articles.forEach(el=>
    {
      let miaCard = document.createElement ('div');
      miaCard.classList.add('sCard');
      miaCard.innerHTML = 
      `
        <h3>${el.subtitle}</h3>
        <p>${el.description}</p>
      
      `
      miaCard.style.background = `url(${el.url})`;
      eContainer.appendChild(miaCard);
    }
    )


  // let eContatore = document.querySelector('#contatore1');
  let eContatore = document.getElementById('contatore1');

  eContatore.innerHTML = `Il numero degli articoli è ${articles.length}`;

  let eContatore2 = document.getElementById('contatore2');
  let eFooter = document.getElementById('footer');
  

  let rowContact = document.createElement('tr');

  eContatore2.appendChild(rowContact);

 
  counter = 0;
  aulab.users.forEach (el =>
    {
      if (el.role == 'authenticated')
      {
          let rowContact = document.createElement('tr');

          rowContact.innerHTML = 
          `
            <td>${el.name}</td>
            
            
          `
          eContatore2.appendChild(rowContact);


          counter++
      }            
    }
    )

eFooter.innerHTML = 
  `
  <tr><td><hr></td></tr>
  <h3>Il totale degli utenti autenticati è: ${counter}</h3>
  `;



  let eBarraNavigazione = document.getElementById('barraNavigazione');
  
  document.addEventListener('scroll',()=>
    {
      let scrolled = window.scrollY;
      console.log(scrolled);
      if (scrolled>200)
        {
          eBarraNavigazione.classList.add('navbar-light','bg-trasparent');
          eBarraNavigazione.classList.remove('navbar-dark','bg-dark');
        }
        else if (scrolled<200)
        {
          eBarraNavigazione.classList.remove('navbar-light','bg-trasparent');
          eBarraNavigazione.classList.add('navbar-dark','bg-dark');
        }
        else if (scrolled==0)
        {
          eBarraNavigazione.classList.remove('navbar-light','bg-trasparent');
          eBarraNavigazione.classList.add('navbar-dark','bg-dark');
        }
        else
        {
          eBarraNavigazione.classList.remove('navbar-light','bg-trasparent');
          eBarraNavigazione.classList.add('navbar-dark','bg-dark');
        }
    } 
  )